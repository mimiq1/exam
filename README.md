# Финальное задание - Сухин Роман Валерьевич | Blog на Flask
## Инструкция по развертыванию

### Часть 1 - Terraform
1. В файле **./terraform/vkcs_provider.tf** вставляем свой login/password от vk cloud:
> provider "vkcs" {
>    
>    username = "login"
>
>    password = "password"
2. Генерируем ssh ключ (ssh-keygen) и кладем публичную его часть по пути **./terraform/keys/id_rsa.pub**.
3. Запускаем командой (находясь в папке *./terraform*):
> terraform apply
4. В результате получим виртуалку с именем user18-exam.

### Часть 2 - Ansible
1. Вставить ip сервера в файл **./ansible/env/dev/inventory**:
> [db]
>
> vm ansible_host=<server_ip> ansible_user=debian
>
> [app]
>
> vm ansible_host=<server_ip> ansible_user=debian
>
> [front]
>
> vm ansible_host=<server_ip> ansible_user=debian
2. Убедиться, что ssh ключ из первой части установлен в системе.
3. Запустить деплой приложения (находять в папке *./ansible*) командой:
> ansible-playbook -i env/dev/inventory site.yml


**NOTE**: Файл site_.yml - версия плейбука без ролей
