# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/vk-cs/vkcs" {
  version     = "0.1.12"
  constraints = "~> 0.1.12"
  hashes = [
    "h1:hzpuWtG1kbvkHICoQ70W0nLeIp/zvapmINLlyRSwxZY=",
  ]
}
